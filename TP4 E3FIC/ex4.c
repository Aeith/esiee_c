#include <stdio.h>

int longueur(char* s);

int main() {
  char* s1 = "Test";
  char* s2 = "Ceci est un test un peu plus long";
  int c1 = longueur(s1);
  int c2 = longueur(s2);
  printf("%s is %d chars long\n", s2, c1);
  printf("%s is %d chars long\n", s2, c2);
  return 0;
}

int longueur(char* s) {
  char* p;
  int count=0;
  for(p=s; *p; p++) {
    count++;
  }
  return count;
}
