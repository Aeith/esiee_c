#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void delOccursC(char x, char t[]);

int main() {
  char s1[] = "Ceci est une phrase";
  char c1 = 'e';

  printf("Phrase de base : %s\n", s1);

  delOccursC(c1, s1);

  printf("Phrase après suppression des caractères %c : %s\n", c1, s1);

  return 0;
}

void delOccursC(char x, char t[]) {
  int i = 0;

  while(t[i] != '\0') {
    if(t[i] == x) {
      int j = i;

      while(t[j] != '\0') {
        t[j]=t[j+1];
        j++;
      }
    } else {
      i++;
    }
  }
}
