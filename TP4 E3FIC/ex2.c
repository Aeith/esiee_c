#include <stdio.h>
#include <stdlib.h>

void ajoute(int* pA, int* pB);

int main() {
  int S[20] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  int T[10] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
  int* pS = S;
  int* pT = T;

  ajoute(pS, pT);

  return 0;
}

void ajoute(int* pA, int* pB) {
  for(int i=10; i<20; i++) {
    pA[i] = pB[i-10];
  }

  for(int j=0; j<20; j++) {
    printf("%d", pA[j]);
  }
}
