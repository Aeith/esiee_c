#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

void nbOccurs(char* t);

int main() {
  char* s = "abrAcadabra";

  nbOccurs(s);

  return 0;
}

void nbOccurs(char* t) {
  int tabOcur[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

  // Get values
  for(int i=0; i<26; i++) {
    // End of string
    if(t[i] == '\0') break;

    // Is lower
    if(t[i] > 96 && t[i] < 123) {
      // Get the index (index-ascii code for 'a')
      tabOcur[t[i]-97]+=1;
    // Upper case 32 lower than lower case
    } else if (t[i] > 64 && t[i] < 91){
      tabOcur[t[i]-65]+=1;
    } else {
      printf("Caractère invalide : %c\n", t[i]);
    }
  }

  // Print results
  for(int j=0; j<26; j++) {
    if(tabOcur[j] > 0) {
      printf("%d occurence(s) de la lettre %c\n", tabOcur[j], j+97);
    }
  }
}
