#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void delOccursW(char w[], char t[]);

int main() {
  char s1[] = "Ceci est une phrase à rallonge est";
  char s2[] = "est";

  printf("Phrase de base : %s\n", s1);

  delOccursW(s2, s1);

  printf("Phrase après suppression de la phrase %s : %s\n", s2, s1);

  return 0;
}

void delOccursW(char w[], char t[]) {
  int i = 0;
  int k = 0;
  int size_w = 3; //sizeof(w)/sizeof(char);
  int started = 0;
  int size_tw = 0;

  // Loop through all t
  while(t[i] != '\0') {
    // When t looks same as w, continue ('started' to confirm for next iterations that we started counting size)
    if(t[i] == w[0] || (started && t[i] == w[size_tw])) {
      started = 1;
      size_tw++;
    }

    // w found in t when size w in t = size w
    if(size_tw == size_w && started) {
      // Remove word
      while(t[k] != '\0') {
        t[i-size_w+k+1] = t[i+k+size_w-1];
        k++;
      }

      break;
    }

    i++;
  }
}
