#include <stdio.h>
#include <stdlib.h>

int palind(char* s);

int main() {
  char* k = "kayak";
  printf("%s est un palindrome ? %d\n", k, palind(k));

  char* j = "jotaro";
  printf("%s est un palindrome ? %d", j, palind(j));

  return 0;
}

int palind(char* s) {
  int max_index;
  for(int i=0; i<10; i++) {
    if(s[i] == '\0') break;
    max_index=i;
  }

  for (int j=0; j<= max_index; j++) {
    if (s[j] != s[max_index-j]) return -1;
  }

  return 1;
}
