#include <stdio.h>

double resistance(double r[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Résistance des éléments du tableau : %lf\n", resistance(tab, 5));

  return 0;
}

double resistance(double r[], int n) {
  double res = 0.0;
  for(int i=0; i<n; i++) {
    if(r[i] != 0) res+=1/r[i];
  }

  return 1/res;
}
