#include <stdio.h>

void affiche_tab(double t[], int n);
double* copie(double destination[], double source[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  double new_tab[5];
  affiche_tab(copie(new_tab, tab, 5), 5);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* copie(double destination[], double source[], int n) {
  for(int i=0; i<n; i++) {
    destination[i] = source[i];
  }

  return destination;
}
