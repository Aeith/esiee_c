#include <stdio.h>
#include <string.h>

int compte(char c, char s[]);

int main() {
  char s[] = "Ceci est une phrase";
  printf("Nombre de '%c' dans '%s' : %d\n", 'e', s, compte('e', s));

  return 0;
}

int compte(char c, char s[]) {
  int count=0;
  for(int i=0; i<(int) strlen(s); i++) {
    if(s[i] == c) count++;
  }

  return count;
}
