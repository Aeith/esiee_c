#include <stdio.h>
#include <stdlib.h>

void affiche_tab(double t[], int n);
double* copie_sec(double destination[], double source[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  double new_tab[5];
  affiche_tab(copie_sec(new_tab, tab, 5), 5);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* copie_sec(double destination[], double source[], int n) {
  // Avoid empty size
  if(n>0) {
    // Temp memory to prevent overlapping
    double* temp_tab = (double*) malloc(sizeof(double) * n);
    for(int i=0; i<n; i++) {
      // Avoid overlapping a=a
      temp_tab[i] = source[i];
      destination[i] = temp_tab[i];
    }
  }

  return destination;
}
