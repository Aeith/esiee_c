#include <stdio.h>

int indice_premier_negatif (double t[], int n);

int main() {
  double tab[] = {1.1, 2.2, 7.7, -1.1, 2.2};
  printf("L'indice du premier élément négatif est : %d\n", indice_premier_negatif(tab, 5));

  return 0;
}

int indice_premier_negatif (double t[], int n) {
  for(int i=0; i<n; i++) {
    if(t[i] < 0.0) return i;
  }

  return -1;
}
