#include <stdio.h>

void affiche_tab(double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 7.7};
  affiche_tab(tab, 4);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}
