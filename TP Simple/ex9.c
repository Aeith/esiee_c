#include <stdio.h>

double valeur_plus_grand (double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Le plus grand élément du tableau est : %lf\n", valeur_plus_grand(tab, 5));

  return 0;
}

double valeur_plus_grand (double t[], int n) {
  double max = t[0];
  for(int i=0; i<n; i++) {
    if(t[i] > max) max=t[i];
  }

  return max;
}
