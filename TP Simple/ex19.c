#include <stdio.h>
#include <string.h>

void repete(char s[], int n);

int main() {
  char s[] = "toto";
  repete(s, 3);

  return 0;
}

void repete(char s[], int n) {
  for(int i=0; i<strlen(s); i++) {
    for(int j=0; j<n; j++) {
      printf("%c", s[i]);
    }
  }
  printf("\n");
}
