#include <stdio.h>
#include <string.h>

int debute_par(char* chaine1, char* chaine2);

int main() {
  printf("ESIEE débute par ES ? : %d\n", debute_par("ESIEE","ES"));

  return 0;
}

int debute_par(char* chaine1, char* chaine2) {
  if(chaine1[0] != chaine2[0]) return 0;
  for(int j=0; j<(int) strlen(chaine2); j++) {
    if(chaine1[j] != chaine2[j]) return 0;
  }

  return 1;
}
