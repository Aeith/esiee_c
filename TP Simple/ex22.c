#include <stdio.h>
#include <string.h>

int presence(char* chaine1, char* chaine2);

int main() {
  printf("Indice de ES dans ESIEE : %d\n", presence("ESIEE","ES"));

  return 0;
}

int presence(char* chaine1, char* chaine2) {
  for(int i=0; i<(int) strlen(chaine1); i++) {
    if(chaine1[i] == chaine2[i]) {
      for(int j=0; j<(int) strlen(chaine2); j++) {
        if(chaine1[j] != chaine2[j]) return 1;
      }
      return i;
    }
  }

  return 1;
}
