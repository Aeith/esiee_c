#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void affiche_tab(double t[], int n);
double* remplit_alea(double t[], int n);

int main() {
  double tab[5];
  affiche_tab(remplit_alea(tab, 5), 5);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* remplit_alea(double t[], int n) {
  srand (time(NULL));
  for(int i=0; i<n; i++) {
    t[i] = (double) rand() / (double) RAND_MAX;
  }

  return t;
}
