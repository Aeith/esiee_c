#include <stdio.h>

void affiche_tab(double t[], int n);
double* decale_droite(double t[], int k, int d, int f);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  affiche_tab(decale_droite(tab, 2, 0, 2), 5);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* decale_droite(double t[], int k, int d, int f) {
  if(d+k <= f) return t;

  for(int i=d; i<=f; i++) {
    t[i+k] = t[i];
  }

  return t;
}
