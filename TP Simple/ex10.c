#include <stdio.h>

int indice_plus_grand(double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Le plus grand élément du tableau est : %d\n", indice_plus_grand(tab, 5));

  return 0;
}

int indice_plus_grand(double t[], int n) {
  int max = t[0];
  int max_pos = 0;
  for(int i=0; i<n; i++) {
    if(t[i] > max) {
      max=t[i];
      max_pos=i;
    }
  }

  return max_pos;
}
