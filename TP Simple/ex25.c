#include <stdio.h>
#include <string.h>

int* frequence(char t[], int n ,int freq[]);

int main() {
  char s[] = "Ceci est une phrase";
  int ascii[256];
  for(int i=0; i<256; i++) {
    ascii[i] = 0;
  }
  frequence(s, 20, ascii);
  printf("Fréquences des caractères ASCII dans %s : \n", s);
  for(int i=0; i<256; i++) {
    if(ascii[i] > 0) printf("%c : %d\n", i, ascii[i]);
  }

  return 0;
}

int* frequence(char t[], int n ,int freq[]) {
  for(int i=0; i<(int) strlen(t); i++) {
    freq[t[i]]++;
  }

  return freq;
}
