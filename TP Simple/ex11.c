#include <stdio.h>

int est_trie(double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 8.3, 7.7};
  printf("Le tableau est trié ? : %d\n", est_trie(tab, 5));

  return 0;
}

int est_trie(double t[], int n) {
  for(int i=0; i<n-1; i++) {
    if(t[i] > t[i+1]) return 0;
  }

  return 1;
}
