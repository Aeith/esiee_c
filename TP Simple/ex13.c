#include <stdio.h>

int rech_dicho_tab_trie(double x, double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Position de 6.6 dans le tableau : %d\n", rech_dicho_tab_trie(6.6, tab, 5));

  return 0;
}

int rech_dicho_tab_trie(double x, double t[], int n) {
  int i=0;
  int found=0;
  int half_index;

  while(found==0 && i<=n) {
    half_index = (n+i)/2;

    // Same value
    if(t[half_index] == x) found=1;
    // Higher half
    else if (x>t[half_index]) i=half_index+1;
    // Lower half
    else n=half_index-1;
  }

  return half_index;
}
