#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void affiche_tab(double t[], int n);
double* remplit_trie(double t[], int n);

int main() {
  double tab[5];
  affiche_tab(remplit_trie(tab, 5), 5);

  return 0;
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* remplit_trie(double t[], int n) {
  double temp[n];
  srand (time(NULL));

  for(int i=0; i<n; i++) {
    temp[i] = (double) rand() / (double) RAND_MAX;
  }

  for(int i=0; i<n; i++) {
    t[i] = temp[i];
    for(int j=0; j<n; j++) {
      double s = 0.0;
      if(temp[j] > temp[j+1]) {
        s = temp[j];
        t[j] = temp[j+1];
        temp[j+1] = s;
      }
    }
  }

  return t;
}
