#include <stdio.h>

double somme(double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Somme des éléments de tab : %lf\n", somme(tab, 5));

  return 0;
}

double somme(double t[], int n) {
  double sum = 0.0;
  for(int i=0; i<n; i++) {
    sum += t[i];
  }

  return sum;
}
