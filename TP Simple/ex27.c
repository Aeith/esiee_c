#include <stdio.h>
#include <string.h>
                                                                                //#include <wchar.h>
#include <locale.h>

char* majuscule(char s[]);

int main() {
  setlocale(LC_CTYPE, "");

                                                                                //wchar_t star = '';
                                                                                //wchar_t s[] = { 0x2605,  };
  char s[] = "toto à é ñ ô û ç ŷ ï æ œ";
  printf("'toto à é ñ ô û ç ŷ ï æ œ' en majuscule : %s\n", majuscule(s));

  return 0;
}

char* majuscule(char s[]) {
  for(int i=0; i<(int) strlen(s); i++) {
                                                                                //wprintf(L"%lc\n", s[i]);
    // Lower alpha chars
    if(s[i] > 96 && s[i] < 123) {
      s[i]-=32;
      continue;
    }

    // Accents
    if(s[i] > 128 && s[i] < 166) {
      // A
      if(s[i] == 160 || (s[i] > 130 && s[i] < 135)) s[i]='A';
      // C
      if(s[i] == 135) s[i]='C';
      // E
      if(s[i] == 130 || (s[i] > 135 && s[i] < 139)) s[i]='E';
      // I
      if(s[i] == 161 || (s[i] > 138 && s[i] < 142)) s[i]='I';
      // N
      if(s[i] == 164) s[i]='N';
      // O
      if(s[i] == 162 || (s[i] > 146 && s[i] < 150)) s[i]='O';
      // U
      if(s[i] == 163 || (s[i] > 149 && s[i] < 152)) s[i]='U';
      // Y
      if(s[i] == 152) s[i]='Y';

      //Æ
      if(s[i] == 145) s[i]=198;
      //Œ
      if(s[i] == 156) s[i]=140;
    }
  }

  return s;
}
