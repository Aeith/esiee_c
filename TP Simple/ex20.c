#include <stdio.h>
#include <string.h>

char* miroir(char s[]);

int main() {
  char s[] = "toto";
  printf("%s\n", miroir(s));

  return 0;
}

char* miroir(char s[]) {
  char temp;
  int size = strlen(s);
  printf("Size : %d\n", size);
  for(int i=0; i<(size/2); i++) {
    temp = s[i];
    s[i] = s[size-i-1];
    s[size-i-1] = temp;
  }

  return s;
}
