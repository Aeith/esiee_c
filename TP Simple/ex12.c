#include <stdio.h>

int rech_seq_tab_trie(double x, double t[], int n);

int main() {
  double tab[] = {0.0, 1.1, 2.2, 3.3, 7.7};
  printf("Position de 6.6 dans le tableau : %d\n", rech_seq_tab_trie(6.6, tab, 5));

  return 0;
}

int rech_seq_tab_trie(double x, double t[], int n) {
  for(int i=0; i<n; i++) {
    if(t[i] > x) return i;
  }

  return n+1;
}
