#include <stdio.h>
#include <string.h>

char* cherche_remplace(char c, char r, char s[]);

int main() {
  char s[] = "Ceci est une phrase";
  printf("'Ceci est une phrase' avec les 'e' remplacés par 'r' : %s\n", cherche_remplace('e', 'r', s));

  return 0;
}

char* cherche_remplace(char c, char r, char s[]) {
  for(int i=0; i<(int) strlen(s); i++) {
    if(s[i] == c) s[i]=r;
  }

  return s;
}
