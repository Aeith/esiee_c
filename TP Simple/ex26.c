#include <stdio.h>
#include <string.h>

int chaine_vers_entier (char s[]);

int main() {
  char s[] = "123";
  printf("%s en entier : %d\n", s, chaine_vers_entier(s));

  return 0;
}

int chaine_vers_entier (char s[]) {
  int entier=0;
  int power=0;
  for(int i=0; i<(int) strlen(s); i++) {
    power=s[i]-48;

    // Pow method cause it bugged on my computer
    for(int j=0; j<(int) strlen(s)-i-1; j++) {
      power*=10;
    }
    if(power==0) power=1;

    entier+=power;
  }

  return entier;
}
