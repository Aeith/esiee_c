#include <stdio.h>

void affiche_tab(double t[], int n);
double* raz(double t[], int n);

int main() {
  double tab[4];
  affiche_tab(raz(tab, 4), 4);
}

void affiche_tab(double t[], int n) {
  for(int i=0; i<n; i++) {
    printf("%lf\n", t[i]);
  }
}

double* raz(double t[], int n) {
  for(int i=0; i<n; i++) {
    t[i]=0.0;
  }

  return t;
}
