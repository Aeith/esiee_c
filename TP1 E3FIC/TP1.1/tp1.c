#include <stdio.h>
#include <ctype.h>

// Prototypes
void menu();
double somme(double pN);
double avg(double pN);
void testCaract(char pC);
void codesAscii();
double conversionFC(double pN);

int main() {
  double pN = 7;
  double f = 128.6;

  // Menu
  menu();

  // Sum
  double sumPN = somme(pN);
  printf("Sum of the numbers : %lf\n", sumPN);

  // Avg
  double avgPn = avg(pN);
  printf("Avg of the numbers : %lf\n", avgPn);

  // Check char
  testCaract('A');
  testCaract('a');
  testCaract('0');
  testCaract('\n');
  testCaract('\t');

  // ASCII
  codesAscii();

  // Fahrenheit to Celsius
  printf("%lf° Fahrenheit = %lf° Celsius\n", f, conversionFC(f));

  return 0;
}

void menu() {
  int choice;

  do {
    // Menu and user input
    printf("Que voulez-vous faire ?\n\n2)   somme\n3)   avg\n4)    testCaract\nq)   Quitter\n\n");
    scanf("%d", &choice);

    switch(choice) {
      case 2:
        printf("Somme\n");
          break;
      case 3:
        printf("Avg\n");
          break;
      case 4:
        printf("Test carac\n");
          break;
      default:
        printf("Valeur incorrecte\n");
        break;
    }
  } while(choice != 'Q' && choice != 'q');

  printf("Fin du programme");
}

// Return the sum of the pN numbers from user inputs
double somme(double pN) {
  double sum = 0;
  double input = 0;

  // Loop through all pN numbers
  for(int i=0; i < pN; i++) {
    printf("Entrez le %dième nombre : ", i+1);
    scanf("%lf\n", &input);
    sum+=input;
  }
  return sum;
}

// Return the avg of pN numbers from user inputs
double avg(double pN) {
  return somme(pN)/pN;
}

// Test char input type
void testCaract(char pC) {
  if(isalnum(pC)) {
    if(isalpha(pC)) {
      if(isupper(pC)) {
        printf("%c est une majuscule !\n", pC);
      } else {
        printf("%c est une minuscule !\n", pC);
      }
    } else {
      printf("%c est un chiffre !\n", pC);
    }
  } else {
    if (pC == '\n') {
      printf("Le caractère est un retour chariot !\n");
    } else {
      printf("Le caractère est d'un autre type !\n");
    }
  }
}

// Print all ASCII chars
void codesAscii() {
  for(int i=0; i<=255; i++) {
    printf("Caractère ASCII n°%d : %c\n", i, i);
  }
}

// Convert Fahrenheit in Celsius
double conversionFC(double pN) {
  return (pN-32)/1.8;
}
