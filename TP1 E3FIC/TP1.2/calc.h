#include <stdio.h>

#include "classic.c"
#include "etendu.c"
#include "logic.c"

double calc(int pOp1, char pOperateur, int pOp2) {
  switch(pOperateur) {
    case '+':
      return add(pOp1, pOp2);
    case '-':
      return minus(pOp1, pOp2);
    case '*':
      return mult(pOp1, pOp2);
    case '/':
      return div(pOp1, pOp2);
    case '%':
      return mod(pOp1, pOp2);
    case '!':
      return fact(pOp1);
    case 'E':
      return puiss(pOp1, pOp2);
    case '&':
      return ET(pOp1, pOp2);
    case '~':
      return NON(pOp1, pOp2);
    case '|':
      return OU(pOp1, pOp2);
    case '^':
      return OUEX(pOp1, pOp2);
    default:
      return pOp1 == pOp2;
  }
}
