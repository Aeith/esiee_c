#include <stdio.h>

#include "calc.h"

int main() {
  printf("%lf\n", calc(5, '+', 2));
  printf("%lf\n", calc(5, '-', 2));
  printf("%lf\n", calc(5, '*', 2));
  printf("%lf\n", calc(5, '/', 2));
  printf("%lf\n", calc(5, '%', 2));
  printf("%lf\n", calc(5, 'E', 2));
  printf("%lf\n", calc(5, '!', 2));
  printf("%lf\n", calc(5, '&', 2));
  printf("%lf\n", calc(5, '|', 2));
  printf("%lf\n", calc(5, '^', 2));
  printf("%lf\n", calc(5, '~', 2));
  printf("%lf\n", calc(5, '?', 2));
  printf("%lf\n", calc(5, '?', 5));
  return 0;
}
