int OU(int a, int n) {
  return a || n;
}

int ET(int a, int n) {
  return a && n;
}

int NON(int a, int n) {
  return !a;
}

int OUEX(int a, int n) {
  return a ^ n;
}
