#include <math.h>

int mod(int pA, int pB) {
  return pA%pB;
}

int puiss(int X, int n) {
  int sum=X;
  for(int i=1; i<n; i++) {
    sum*=X;
  }
  return sum;
}

int fact(int N) {
  int sum=N;
  for(int i=1; i<N; i++) {
    sum*=N-i;
  }
  return sum;
}
