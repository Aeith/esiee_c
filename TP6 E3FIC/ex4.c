#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if(argc == 2) {
    char* file_name = argv[1];
    FILE* fp = fopen(file_name, "r");
    size_t n = 0;
    int c, phase = 0;
    int count_lines = 0, count_points = 0, count_questions = 0, count_exclamations = 0, count_tabulations = 0;

    // Loop file
    if(fp == NULL) exit(1);
    while((c = fgetc(fp)) != EOF) {
      if(c == '.' && phase) count_points++;
      if(c == '?' && phase) count_questions++;
      if(c == '!' && phase) count_exclamations++;
      if(c != '.' && c != '?' && c != '!') phase = 1; else phase = 0;
      if(c == '\t') count_tabulations++;
      if(c == '\n') count_lines++;
    }

    // Show
    printf("Nombre de phrases terminées par des points : %d des points d'interrogation : %d d'exclamations ! %d\n", count_points, count_questions, count_exclamations);
    printf("Nombre de lignes : %d\n", count_lines);
    printf("Nombre de tabulations : %d\n", count_tabulations);

    // Clear memory
    fclose(fp);
    return 0;
  } else {
    if(argc <= 1) {
      printf("Manque d'arguments\n");
    } else {
      printf("Trop d'arguments\n");
    }
    exit(1);
  }
}
