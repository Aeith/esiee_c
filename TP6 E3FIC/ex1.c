#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void write_form(char* entries);
char* get_entry();

char* file_name;

int main(int argc, char **argv) {
  file_name = argv[2];
  write_form(argv[1]);

  return 0;
}

// Write N entries in file
void write_form(char* n) {
  FILE* fp = fopen(file_name, "a");

  // String to int
  long int entries;
  entries = strtol(n, NULL, 10);

  // Write all entries
  for(int i=0; i<entries; i++) {
    int matricule;
    double note, salary;
    char firstname[10], lastname[10];

    printf("\nEntrez le numéro de matricule :");
    scanf("%d", &matricule);

    printf("\nEntrez le nom puis le prénom :");
    scanf("%s %s", firstname, lastname);

    printf("\nEntez la note et le salaire :");
    scanf("%lf %lf", &note, &salary);

    fprintf(fp, "Matricule %d | Prénom %s | Nom %s | Note %lf | Salaire %lf\n", matricule, firstname, lastname, note, salary);
  }

  fclose(fp);
}
