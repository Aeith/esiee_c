#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if(argc >= 2) {
    FILE* fp = fopen("nouveau_fichier.txt", "a");

    // Loop arguments
    for(int i=1; i<argc; i++) {
      for(int j=0; j<strlen(argv[i]); j++) {
        fputc(argv[i][j], fp);
      }
      fputc('\n', fp);
    }

    // Clear memory
    fclose(fp);
    return 0;
  } else {
    printf("Manque d'arguments\n");
    exit(1);
  }
}
