#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void write_form(char* n);
void read_file(char* n);

char* file_name;

int main(int argc, char **argv) {
  file_name = argv[2];
  write_form(argv[1]);

  read_file(argv[1]);

  return 0;
}

// Write N entries in file
void write_form(char* n) {
  FILE* fp = fopen(file_name, "a");

  // String to int
  long int entries;
  entries = strtol(n, NULL, 10);

  // Write all entries
  for(int i=0; i<entries; i++) {
    // Init values
    int matricule, age;
    double note, salary, prime;
    char name[10];
    char result[200];
    char m[10], a[3], n[5], s[10], p[10];

    // Get values
    printf("\nEntrez le numéro de matricule :");
    scanf("%d", &matricule);

    printf("\nEntrez le nom puis l'âge :");
    scanf("%s %d", name, &age);

    printf("\nEntez la note, le salaire et la prime :");
    scanf("%lf %lf %lf", &note, &salary, &prime);

    // Int to string
    sprintf(m, "%d", matricule);
    sprintf(a, "%d", age);
    // Long to string
    sprintf(s, "%.2f", salary);
    sprintf(p, "%.2f", prime);
    sprintf(n, "%.2f", note);

    // Concatenation
    strcpy(result, "Matricule : ");
    strcat(result, m);
    strcat(result, " | Nom : ");
    strcat(result, name);
    strcat(result, " | Age : ");
    strcat(result, a);
    strcat(result, " | Note : ");
    strcat(result, n);
    strcat(result, " | Salaire : ");
    strcat(result, s);
    strcat(result, " | Prime : ");
    strcat(result, p);
    strcat(result, "\n");

    fwrite(result, strlen(result), 1, fp);
    fseek(fp, 0, SEEK_SET);
  }

  fclose(fp);
}

void read_file(char* n) {
  FILE* fp=fopen(file_name, "r");

  // String to int
  long int entries;
  entries = strtol(n, NULL, 10);
  char buffer[200*entries];

  fread(buffer, 200*entries, 1, fp);
  printf("%s\n", buffer);
  fclose(fp);
}
