#include <stdio.h>
#include <string.h>

#include "ex2.h"

int main() {
  t_Date d1;
  d1.day = 7;
  d1.month = 5;
  d1.year = 1997;
  t_Date d2;
  d2.day = 10;
  d2.month = 3;
  d2.year = 1997;
  t_Date d3;
  d3.day = 14;
  d3.month = 7;
  d3.year = 1789;

  t_Identite id1;
  strcpy(id1.firstname, "Antoine");
  strcpy(id1.lastname, "BASLÉ");
  t_Identite id2;
  strcpy(id2.firstname, "Margaux");
  strcpy(id2.lastname, "ANDRIEUX");
  t_Identite id3;
  strcpy(id3.firstname, "Axel");
  strcpy(id3.lastname, "BASLÉ");
  t_Identite id4;
  strcpy(id4.firstname, "Jean");
  strcpy(id4.lastname, "JEAN");

  t_Adresse ad1;
  ad1.number = 16;
  strcpy(ad1.street, "Haut Petits Boi");
  ad1.postal = 78600;
  strcpy(ad1.city, "MaisonsLaffitte");
  strcpy(ad1.country, "France");
  t_Adresse ad2;
  ad2.number = 24;
  strcpy(ad2.street, "Dieutre");
  ad2.postal = 76000;
  strcpy(ad2.city, "Rouen");
  strcpy(ad2.country, "France");
  t_Adresse ad3;
  ad3.number = 29;
  strcpy(ad3.street, "Louis Pasteur");
  ad3.postal = 14390;
  strcpy(ad3.city, "Cabourg");
  strcpy(ad3.country, "France");

  t_Contact ct1;
  ct1.phone[0] = 0;
  ct1.phone[1] = 6;
  ct1.phone[2] = 6;
  ct1.phone[3] = 9;
  ct1.phone[4] = 2;
  ct1.phone[5] = 0;
  ct1.phone[6] = 1;
  ct1.phone[7] = 0;
  ct1.phone[8] = 8;
  ct1.phone[9] = 7;
  strcpy(ct1.email, "jean.JEAN@gmail.com");

  Eleve e1;
  e1.naiss = d1;
  e1.id = id1;
  e1.adr = ad1;
  Eleve e2;
  e2.naiss = d1;
  e2.id = id2;
  e2.adr = ad1;
  Eleve e3;
  e3.naiss = d2;
  e3.id = id3;
  e3.adr = ad2;
  Eleve e4;
  e4.naiss = d3;
  e4.id = id4;
  e4.adr = ad3;
  e4.ctact = ct1;

  printf("Dates de naissances égales entre e1 et e2 ? %d\n", cmpDates(e1.naiss, e2.naiss));
  printf("Dates de naissances égales entre e1 et e3 ? %d\n", cmpDates(e1.naiss, e3.naiss));

  printf("\nIdentités égales entre e1 et e1 ? %d\n", cmpIdentites(e1.id, e1.id));
  printf("Identités égales égales entre e1 et e3 ? %d\n", cmpIdentites(e1.id, e3.id));

  printf("\nAdresses égales entre e1 et e2 ? %d\n", cmpAdresse(e1.adr, e1.adr));
  printf("Adresses égales égales entre e1 et e3 ? %d\n", cmpAdresse(e1.adr, e3.adr));

  saisieEleve(e1);
  saisieEleve(e2);
  saisieEleve(e3);

  afficherEleve(e4);

  printf("\nÉlève e1 dans annuraire ? %d\n", SearchEleve(e1));
  printf("\nÉlève e4 dans annuraire ? %d\n", SearchEleve(e4));

  return 0;
}

// Return 1 if both dates are equal
int cmpDates(t_Date d1, t_Date d2) {
  if((d1.day == d2.day) && (d1.month == d2.month) && (d1.year == d2.year)) {
    return 1;
  }
  return 0;
}

// Return 1 if both identities are equal
int cmpIdentites(t_Identite id1, t_Identite id2) {
  if((strcmp(id1.firstname, id2.firstname) == 0) && (strcmp(id1.lastname, id2.lastname) == 0)) {
    return 1;
  }
  return 0;
}

// Return 1 if both addresses are equal
int cmpAdresse(t_Adresse ad1, t_Adresse ad2) {
  if((ad1.number == ad2.number) && (strcmp(ad1.street, ad2.street) == 0) && (ad1.postal == ad2.postal) && (strcmp(ad1.city, ad2.city) == 0) && (strcmp(ad1.country, ad2.country) == 0)) {
    return 1;
  }
  return 0;
}

// Add eleve to annuaire
void saisieEleve(Eleve e1) {
  annuaire.eleves[annuaire.count] = e1;
  annuaire.count += 1;
}

// Print e1 data
void afficherEleve(Eleve e1) {
  printf("Informations sur l'élève :\n");
  printf("\nIdentité :\nPrénom %s\nNom %s\n", e1.id.firstname, e1.id.lastname);
  printf("\nNaissance : %d/%d/%d\n", e1.naiss.day, e1.naiss.month, e1.naiss.year);
  printf("\nAdresse : \n%d %s\n%d %s %s\n", e1.adr.number, e1.adr.street, e1.adr.postal, e1.adr.city, e1.adr.country);
  printf("\nContact : \nTéléphone ");
  for(int i=0; i < 10; i++) {
    printf("%d", e1.ctact.phone[i]);
  }
  printf("\nMail %s\n", e1.ctact.email);
}

// Return 1 if found in annuaire
int SearchEleve(Eleve e1) {
  for(int i=0; i < annuaire.count; i++) {
    Eleve currentStudent = annuaire.eleves[i];
    if((cmpDates(e1.naiss, currentStudent.naiss) == 1) && (cmpIdentites(e1.id, currentStudent.id) == 1) && (cmpAdresse(e1.adr, currentStudent.adr) == 1)) {
      return 1;
    }
  }
  return 0;
}
