typedef struct t_Date {
   int day;
   int month;
   int year;
} t_Date;

typedef struct t_Identite {
  char firstname[20];
  char lastname[20];
} t_Identite;

typedef struct t_Adresse {
  int number;
  char street[15];
  int postal;
  char city[15];
  char country[15];
} t_Adresse;

typedef struct t_Contact {
  int phone[10];
  char email[15];
} t_Contact;

typedef struct Eleve {
  t_Identite id;
  t_Date naiss;
  t_Adresse adr;
  t_Contact ctact;
} Eleve;

typedef struct Annuaire {
  Eleve eleves[10];
  int count;
} Annuaire;

Annuaire annuaire = {
  .count = 0
};

int cmpDates(t_Date d1, t_Date d2);
int cmpIdentites(t_Identite id1, t_Identite id2);
int cmpAdresse(t_Adresse ad1, t_Adresse ad2);
void saisieEleve(Eleve e1);
void afficherEleve(Eleve e1);
int SearchEleve(Eleve e1);
