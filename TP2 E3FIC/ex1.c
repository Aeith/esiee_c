#include <stdio.h>
#include <math.h>

#include "ex1.h"

int main() {
   printf("Initial tab\n");
   double tab[] = {1.1, 1.1, -2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};
   affiche_tab(tab, 5);

   printf("\nEmptied for the first element\n");
   init(tab, 1);
   affiche_tab(tab, 5);

   printf("\nSum of the first six elements\n");
   printf("%lf\n", somme(tab, 6));

   printf("\nAvg of the first six elements\n");
   printf("%lf\n", moyenne(tab, 6));

   printf("\nFirst index of the first three elements\n");
   printf("%d\n", indice_premier_negatif(tab, 3));

   printf("\nMax element of the first nine elements\n");
   printf("%lf\n", valeur_plus_grand(tab, 9));

   printf("\nIndex of max element in the first nine elements\n");
   printf("%d\n", indice_plus_grand(tab, 9));

   printf("\nAre the first two elements in order ?\n");
   printf("%d\n", ordonne(tab, 2));
   printf("\nAre the first three elements in order ?\n");
   printf("%d\n", ordonne(tab, 3));

   printf("\nAdded 7.7 as the second element\n");
   ajoutAukieme(tab, 1, 7.7);
   affiche_tab(tab, 10);

   printf("\nRemoved the first element\n");
   supprime_debut(tab);
   affiche_tab(tab, 9);

   printf("\nAdded 0.0 at the end\n");
   ajout_fin(tab, 0.0);
   affiche_tab(tab, 10);

   printf("\nRemoved the seventh element\n");
   supprime_index(tab, 6);
   affiche_tab(tab, 9);

   return 0;
}

// Print the n first elements of t
void affiche_tab(double t[], int n) {
  for(int i=0; i < n; i++) {
    printf("%lf\n", t[i]);
  }
}

// Fill t with n*0.0
void init(double t[], int n) {
  for(int i=0; i < n; i++) {
    t[i] = 0.0;
  }
}

// Return the sum of the n first values of t
double somme(double t[], int n) {
  double sum=0;
  for(int i=0; i < n; i++) {
    sum+=t[i];
  }
  return sum;
}

// Return the average of the n first values of t
double moyenne(double t[], int n) {
  return somme(t, n)/n;
}

// Return the first index of the first negative elements of t from the Nth first elements
int indice_premier_negatif(double t[], int n) {
  for(int i = 0; i < n; i++) {
    if(t[i] < 0) {
      return i;
    }
  }

  return -1;
}

// Return the max number of the Nth first elements of t
double valeur_plus_grand(double t[], int n) {
  double max=t[0];
  for(int i = 0; i < n; i++) {
    if(t[i] > max) {
      max = t[i];
    }
  }
  return max;
}

// Return the max number index of the Nth first elements of t
int indice_plus_grand(double t[], int n) {
  double max=t[0];
  int max_index=0;
  for(int i = 0; i < n; i++) {
    if(t[i] > max) {
      max = t[i];
      max_index=i;
    }
  }
  return max_index;
}

// Return 1 if the Nth elements are ascending or 0 otherwise
int ordonne(double t[], int n) {
  for(int i=0; i < n; i++) {
    if(fabs(t[i]-t[i-1]) < 0.00001) return 0;
  }

  return 1;
}

// Add E element in the k position of the t array
void ajoutAukieme(double t[], int k, double e) {
  int found=0;
  double current;
  double last;
  for(int i=0; i < 10; i++) {
    if(found==1) {
      current = t[i];
      t[i] = last;
      last = current;
    }

    if(i==k) {
      found = 1;
      last = t[i];
      t[i] = e;
    }
  }
}

// Remove the first element
void supprime_debut(double t[]) {
  for(int i=0; i < 9; i++){
    t[i] = t[i+1];
  }
}

// Add element at the end
void ajout_fin(double t[], double n) {
  t[9] = n;
}

// Remove element at the n index
void supprime_index(double t[], int k) {
  int found=0;
  for(int i=0; i < 9; i++){
    if(i==k) {
      found = 1;
    }

    if(found==1) {
      t[i] = t[i+1];
    }
  }
}
