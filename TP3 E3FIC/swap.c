#include <stdio.h>
#include <stdlib.h>

void swap(int* pA, int* pB);

int main() {
  int x=20, y=44;
  int* p1;
  int* p2;
  p1 = &x;
  p2 = &y;

  printf("P1 : %d\n", *p1);
  printf("P2 : %d\n", *p2);

  swap(p1, p2);

  printf("P1 : %d\n", *p1);
  printf("P2 : %d\n", *p2);

  return 0;
}

void swap(int *pA, int *pB) {
  int tmp = *pB;
  (*pB) = (*pA);
  (*pA) = tmp;
}
