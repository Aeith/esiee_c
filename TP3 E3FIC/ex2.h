typedef struct t_Date {
  int day;
  int month;
  int year;
} t_Date;

typedef struct t_Identite {
  char firstname[15];
  char lastname[15];
} t_Identite;

typedef struct t_Eleve {
  t_Identite id;
  t_Date naiss;
} t_Eleve;

typedef struct Annuaire {
  t_Eleve* eleves[20];
  int cpt;
} Annuaire;

Annuaire annuaire = {
  .cpt = 0
};

int cmpDates(t_Date d1, t_Date d2);
int cmpIdentites(t_Identite id1, t_Identite id2);
t_Eleve* saisie_Eleve(t_Eleve* e);
void afficher_Eleve(t_Eleve* e);
int Search_Eleve(t_Eleve e1);
