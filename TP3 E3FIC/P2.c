#include <stdio.h>
#include <stdlib.h>

int main(){
  /* Cas statique */
  int i = 2;
  int * ptr;
  ptr= &i;
  *ptr= 3;
  printf(" entrez valeur de i :");
  scanf("%d", &i);
  printf(" entrez valeur de ptr :");
  scanf("%d", *ptr);
  /* Cas dynamique */
  int *tab= (int *)malloc(sizeof(int)); /* allocation d'un entier */
  *ptr= 3;
  int * p = (int*)malloc(5*sizeof(int)); // permet d'allouer une zone mémoire pouvant contenir 5 entiers
  return 0;
}
