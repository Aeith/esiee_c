#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ex2.h"

int main() {
  t_Date d1;
  d1.day = 7;
  d1.month = 5;
  d1.year = 1997;
  t_Date d2;
  d2.day = 10;
  d2.month = 3;
  d2.year = 1997;

  t_Identite id1;
  strcpy(id1.firstname, "Antoine");
  strcpy(id1.lastname, "BASLÉ");
  t_Identite id2;
  strcpy(id2.firstname, "Margaux");
  strcpy(id2.lastname, "BASLÉ");

  printf("Dates de naissances égales entre d1 et d1 ? %d\n", cmpDates(d1, d1));
  printf("Dates de naissances égales entre d1 et d2 ? %d\n", cmpDates(d1, d2));

  printf("\nIdentités égales entre id1 et id1 ? %d\n", cmpIdentites(id1, id1));
  printf("Identités égales égales entre id1 et id2 ? %d\n", cmpIdentites(id1, id2));

  t_Eleve* e1, eleve1;
  e1 = &eleve1;
  e1->naiss = d1;
  e1->id = id1;

  t_Eleve* e2;
  e2 = saisie_Eleve(e2);
  afficher_Eleve(e2);

  t_Eleve* e3;
  e3 = saisie_Eleve(e3);
  afficher_Eleve(e3);

  printf("\nÉlève e1 dans l'annuaire ? %d\n", Search_Eleve(*e1));
  printf("\nÉlève e2 dans l'annuaire ? %d\n", Search_Eleve(*e2));

  return 0;
}

// Return 1 if both dates are equal
int cmpDates(t_Date d1, t_Date d2) {
  if((d1.day == d2.day) && (d1.month == d2.month) && (d1.year == d2.year)) {
    return 1;
  }
  return 0;
}

// Return 1 if both identities are equal
int cmpIdentites(t_Identite id1, t_Identite id2) {
  if((strcmp(id1.firstname, id2.firstname) == 0) && (strcmp(id1.lastname, id2.lastname) == 0)) {
    return 1;
  }
  return 0;
}

// Add a new student
t_Eleve* saisie_Eleve(t_Eleve* e) {
  // Dynamic memory allocation
  t_Eleve* eleve = (t_Eleve*) malloc(sizeof(t_Eleve *));

  /* PREPARE values */
  int day, month, year;
  char firstname[15], lastname[15];
  t_Identite id;
  t_Date naissance;

  /* GET values */
  printf("\nEntrez le prénom et le nom de l'élève : ");
  scanf("%s %s", firstname, lastname);

  printf("Entrez le jour, le mois et l'année de naissance de l'élève : ");
  scanf("%d %d %d", &day, &month, &year);

  /* SET values */
  strcpy(id.firstname, firstname);
  strcpy(id.lastname, lastname);

  naissance.day = day;
  naissance.month = month;
  naissance.year = year;

  eleve->id = id;
  eleve->naiss = naissance;

  // Update annuaire
  annuaire.eleves[annuaire.cpt] = eleve;
  annuaire.cpt += 1;

  return eleve;
}

// Display el data
void afficher_Eleve(t_Eleve* e) {
  printf("Informations sur l'élève :\n");
  printf("\nIdentité :\nPrénom %s\nNom %s\n", e->id.firstname, e->id.lastname);
  printf("Date de naissance :\n%d/%d/%d\n", e->naiss.day, e->naiss.month, e->naiss.year);
}

// Return 0 if found in annuaire
int Search_Eleve(t_Eleve e1) {
  for(int i=0; i < annuaire.cpt; i++) {
    t_Eleve currentStudent = *annuaire.eleves[i];
    if((cmpDates(e1.naiss, currentStudent.naiss) == 1) && (cmpIdentites(e1.id, currentStudent.id) == 1)) {
      return 0;
    }
  }
  return -1;
}
