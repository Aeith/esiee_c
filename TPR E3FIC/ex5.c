#include <stdio.h>

int nb_partition(int n, int t);

int main() {
  printf("Nombre de partitions(7, 7) : %d\n", nb_partition(6, 5));

  return 0;
}

int nb_partition(int n, int t) {
  if (n==0) return 1;
  if (n<0) return 0;
  if (n>0 && t==1)  return 1;
  if (n>0 && t>1) {
    return nb_partition(n-t, t)+nb_partition(n, t-1);
  } return 0;
}
