#include <stdio.h>

int nb_rec(int s, int p[], int n);

int main() {
  int test[] = {5, 10, 20};
  printf("Nombre de façons de payer 35€ avec 5, 10 et 20€ : %d\n", nb_rec(35, test, 3));
  int all[] = {1, 2, 5, 10, 20, 50, 100, 200, 500};
  printf("Nombre de façons de payer 100€ avec 1, 2, 5, 10, 20, 50, 100, 200 et 500€ : %d\n", nb_rec(100, all, 9));
  int small[] = {1, 2, 5, 10};
  printf("Nombre de façons de payer 2018€ avec 1, 2, 5 et 10€ : %d\n", nb_rec(2018, small, 4));

  return 0;
}

int nb_rec(int s, int p[], int n) {
  if(s==0) return 1;
  if(s<0 || n==0) return 0;
  int max=p[0];
  int max_index=0;
  int new_p[n-1];

  // Get G
  for(int i=0; i<n; i++) {
    if(p[i] > max && p[i] <= s) {
      max=p[i];
      max_index=i;
    }
  }

  // Remove G from P
  for(int j=0; j<n-1; j++) {
    if(j>=max_index) new_p[j] = p[j+1];
    else new_p[j] = p[j];
  }

  return nb_rec(s-max, p, n)+nb_rec(s, new_p, n-1);
}
