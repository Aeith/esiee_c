int est_bissextile(int annee);
int nb_jours_par_an(int annee);
int nb_jours_par_mois(int mois, int annee);
int ecart_dates(int j1, int m1, int a1, int j2, int m2, int a2);
void nom_jour(int j, int m, int a);
