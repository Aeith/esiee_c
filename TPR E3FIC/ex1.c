#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include "ex1.h"

int main() {
  //printf("Ecart de date entre le 10 mars 1997 et le 7 mai 1997 : %d\n", ecart_dates(10, 3, 1997, 7, 5, 1997));
  nom_jour(18, 9, 1997);

  return 0;
}

int est_bissextile(int annee) {
  if (annee%4 == 0) {
    if (annee%100 == 0) {
      if (annee%400 == 0) {
        return 1;
      } else {
          //printf("L'année %d n'est pas une année bissextile car elle n'est pas divisible par 400\n", annee);
          return 0;
      }
    } else {
      //printf("L'année %d n'est pas une année bissextile car elle n'est pas divisible par 100\n", annee);
      return 0;
    }
  } else {
    //printf("L'année %d n'est pas une année bissextile car elle n'est pas divisible par 4\n", annee);
    return 0;
  }
}

int nb_jours_par_an(int annee) {
  if(est_bissextile(annee)) {
    return 366;
  }
  return 365;
}

int nb_jours_par_mois(int mois, int annee) {
  int nb_jours = 0;
  int months[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
  if (mois > 0 && mois <= 12) {
    nb_jours = months[mois-1];
    // Add 1 day to february
    if (est_bissextile(annee) && mois == 2) {
      nb_jours++;
    }
    return nb_jours;
  } else {
    return 0;
  }
}

int ecart_dates(int j1, int m1, int a1, int j2, int m2, int a2) {
  int ecart_annees=a1-a2;
  int ecart_mois=m1-m2;
  int ecart_jour=0-j1+j2;

  for(int i = 0; i < fabs(ecart_annees); i++) {
    if(a1-a2<0)
      ecart_jour+=nb_jours_par_an(a2-i);
    else
      ecart_jour+=nb_jours_par_an(a1+i);
  }

  for(int i = fabs(ecart_mois); i > 0; i--) {
    if(m1-m2<0) {
      ecart_jour+=nb_jours_par_mois(m2-i, a2);
    } else
      ecart_jour+=nb_jours_par_mois(m1+i, a1);
  }

  return ecart_jour;
}

void nom_jour(int j, int m, int a) {
  int ecart = ecart_dates(j, m, a, 1, 1, 2000);
  int day = ecart%7;

  char* day_name;
  switch(day) {
    case 0:
      day_name = "Lundi";
      break;
    case 1:
      day_name = "Mardi";
      break;
    case 2:
      day_name = "Mercredi";
      break;
    case 3:
      day_name = "Jeudi";
      break;
    case 4:
      day_name = "Vendredi";
      break;
    case 5:
      day_name = "Samedi";
      break;
    case 6:
      day_name = "Dimanche";
      break;
    default:
      printf("Erreur durant la recherche du nom du jour");
      break;
  }

  printf("Le %d/%d/%d était un %s\n", j, m, a, day_name);
}
