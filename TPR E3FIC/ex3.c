#include <stdio.h>
#include <stdlib.h>

int u0 = 1;
int v0 = 2;
int w0 = 3;

int U(int n);
int V(int n);
int W(int n);

int main() {
  printf("U1 : %d\n", U(1));
  printf("V1 : %d\n", V(1));
  printf("W1 : %d\n", W(1));

  return 0;
}

int U(int n) {
  if(n > 0) {
    return 2*(U(n-1)) + 3*(V(n-1)) + W(n-1);
  }

  return u0;
}

int V(int n) {
  if(n > 0) {
    return U(n-1) + V(n-1) + 2*W(n-1);
  }

  return v0;
}

int W(int n) {
  if(n > 0) {
    return U(n-1) + 4*(V(n-1)) + W(n-1);
  }

  return w0;
}
