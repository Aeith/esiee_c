#include <stdio.h>

int M(int n);
int g(int m, int n);
int pair(int n);
int estPair(int n);
int estImpair(int n);

int main() {
  printf("Mac Carthy 96 = %d\n", M(96));
  printf("G(1, 0) infini\n");
  printf("Pair(7) : %d\n", pair(7));
  printf("Pair(1) ou Impair(0) ? 7 : %d\n", estPair(7));

  return 0;
}

int M(int n) {
  if (n > 100) {
    return n-10;
  } else {
    return M(M(n+11));
  }
}

int g(int m, int n){
  printf("Coucou\n");
  if (m == 0) { printf("Coucou\n"); return 1; }
  else return g(m - 1, g(m, n));
}

int pair(int n) {
  if(n==0) return 1;
  if(n==1) return 0;
  else
    return pair(n-2);
}

int estPair(int n) {
  if (n==0) return 1;
  else return estImpair(n-1);
}

int estImpair(int n) {
  if (n==0) return 0;
  else return estPair(n-1);
}
