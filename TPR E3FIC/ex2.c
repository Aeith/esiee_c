#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int fib(int n);
int fact(int n);
int C(int n, int p);
int Ack(int m, int n);

int main() {
  printf("Fibonacci : %d\n", fib(5));
  printf("Factorielle : %d\n", fact(5));
  printf("Combinaison : %d\n", C(5, 2));
  printf("Ack : %d\n", Ack(2, 5));

  return 0;
}
int fib(int n){
  if(n <= 1) return 1;
  else return fib(n-1) + fib(n-2);
}

int fact(int n) {
  if(n <= 1) return 1;
  else return n * fact (n-1);
}

int C(int n, int p) {
  if((n == 0) || (p == n))
    return 1;
  else
    return C(n-1, p-1) + C(n-1, p);
}

/*int S(int n){
  u = 1; v = 1;
  for(int i = 2; i <= n; ++i){
    u0 = u; v0 = v;
    u = u0 + v0;
    v = v0;
  }
  return u;
}*/

int Ack(int m, int n) {
  if(m==0)
    return n+1;
  else if(n==0)
    return Ack(m-1, 1);
  else
    return Ack(m-1, Ack(m, n-1));
}
