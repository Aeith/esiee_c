#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if(argc > 1 && argc < 4) {
    FILE* fp;
    char* file_name = argv[2];
    char* line=NULL;
    size_t len=0;
    ssize_t read;
    int count_lines = 0, count_chars = 0;

    fp = fopen(file_name, "r");
    if(fp == NULL) exit(1);
    while((read = getline(&line, &len, fp)) != -1) {
      count_lines++;
      count_chars+=read;
    }

    int lines_option=strcmp(argv[1], "-L");
    int chars_option=strcmp(argv[1], "-C");
    if(lines_option == 0 || chars_option == 0) {
      if(lines_option == 0 ) {
        printf("Nombre total de lignes : %d\n", count_lines);
      } else {
        printf("Nombre total de caractères : %d\n", count_chars);
      }
    } else {
      printf("Option invalide\n");
    }

    fclose(fp);
    if(line) free(line);
    exit(1);
  } else {
    if(argc <= 1) {
      printf("Manque d'arguments\n");
    } else {
      printf("Trop d'arguments\n");
    }
    exit(1);
  }
}
