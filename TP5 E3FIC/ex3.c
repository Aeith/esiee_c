#include <stdlib.h>
#include <string.h>

#include "ex3.h"

char* file_name = "Eleves.dat";

int main(int argc, char **argv) {
  menu();
  return 0;
}

// Open file
FILE* ouverture(char* file_name, char* mode) {
  FILE* fp = fopen(file_name, mode);

  if(fp == NULL) exit(1);

  return fp;
}

// Close file
void fermeture(FILE* fp) {
  fclose(fp);
}

void menu() {
  char choix;
  do {
    // Init values
    int matri, day, month, year;
    double moy;
    char fname[10];

    printf("\n\nAjouter d'un nouvel élève...............: A\n");
    printf("Rechercher un eleve par N°matricule.....: S\n");
    printf("Lister tous les eleves...(print)........: P\n");
    printf("Quitter.................................: Q\n");
    printf(" \nFaites votre choix: ");
    scanf("%c",&choix);

    switch(choix)
    {
      case 'a':
      case 'A':
        // Get values
        printf("\nEntrez le matricule de l'élève : ");
        scanf("%d", &matri);
        printf("\nEntrez le nom de l'élève : ");
        scanf("%s", fname);
        printf("\nEntrez le jour, le mois et l'année de naissance de l'élève : ");
        scanf("%d %d %d", &day, &month, &year);
        printf("\nEntrez la moyenne de l'élève : ");
        scanf("%lf", &moy);

        // Set date
        tDate d;
        d.day = day;
        d.month = month;
        d.year = year;

        // Set eleve
        typEleve* e, eleve;
        e = &eleve;
        strcpy(e->name, fname);
        e->matricule = matri;
        e->date = d;
        e->moy = moy;

        ajout(e);
        break;
      case 's':
      case 'S':
        printf("\nEntrez le matricule de l'élève : ");
        scanf("%d", &matri);
        searchEleve(matri);
        break;
      case 'p':
      case 'P':
        lister();
        break;
      default:
        break;
    }
  }  while (choix != 'q' && choix != 'Q');

  exit(0);
}

// Add pE into Eleves.dat
int ajout(typEleve* pE) {
  FILE* fp = ouverture(file_name, "a");

  fprintf(fp, "Eleve : %s | Matricule : %d | Moyenne : %lf | Date naissance : %d/%d/%d\n", pE->name, pE->matricule, pE->moy, pE->date.day, pE->date.month, pE->date.year);

  fermeture(fp);
  return 0;
}

// Search for matricule pNum in Eleves.dat
int searchEleve (int pNum) {
  FILE* fp = ouverture(file_name, "r");
  char* line=NULL;
  size_t len=0;
  ssize_t read;
  char matricule[10];

  while((read = getline(&line, &len, fp)) != -1) {
    // Int to string
    sprintf(matricule, "%d", pNum);

    // Sub string in string
    if(strstr(line, matricule) != NULL) {
      printf("Matricule %s trouvé !\n", matricule);
      fermeture(fp);
      if(line) free(line);
      return 0;
    }
  }

  fermeture(fp);
  if(line) free(line);
  return -1;
}

// Print all eleve in file
void lister() {
  FILE* fp = ouverture(file_name, "r");
  char* line=NULL;
  size_t len=0;
  ssize_t read;

  while((read = getline(&line, &len, fp)) != -1) {
    printf("Elève : %s\n", line);
  }

  fermeture(fp);
  if(line) free(line);
}
