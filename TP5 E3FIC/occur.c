#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void nbOccurs(char* t, int* tabOcur);

int main(int argc, char **argv) {
  if(argc == 2) {
    FILE* fp;
    char* file_name = argv[1];
    char* line=NULL;
    size_t len=0;
    ssize_t read;
    int tabOcur[26] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

    fp = fopen(file_name, "r");
    if(fp == NULL) exit(1);
    while((read = getline(&line, &len, fp)) != -1) {
      nbOccurs(line, tabOcur);
    }

    // Print results
    for(int j=0; j<26; j++) {
      if(tabOcur[j] > 0) {
        printf("%d occurence(s) de la lettre %c\n", tabOcur[j], j+97);
      }
    }

    fclose(fp);
    if(line) free(line);
    exit(1);
  } else {
    if(argc <= 1) {
      printf("Manque d'arguments\n");
    } else {
      printf("Trop d'arguments\n");
    }
    exit(1);
  }
}

void nbOccurs(char* t, int* tabOcur) {
  // Get values
  for(int i=0; i<26; i++) {
    // End of string
    if(t[i] == '\0') break;

    // Is lower
    if(t[i] > 96 && t[i] < 123) {
      // Get the index (index-ascii code for 'a')
      tabOcur[t[i]-97]+=1;
    // Upper case 32 lower than lower case
    } else if (t[i] > 64 && t[i] < 91){
      tabOcur[t[i]-65]+=1;
    } else {
      printf("Caractère invalide : %c\n", t[i]);
    }
  }
}
