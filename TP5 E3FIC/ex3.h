#include <stdio.h>

typedef struct tDate {
  int day;
  int month;
  int year;
} tDate;

typedef struct typEleve {
  int matricule;
  char name[10];
  tDate date;
  double moy;
} typEleve;

FILE* ouverture(char* file_name, char* mode);
void fermeture(FILE* fp);
void menu();
int ajout(typEleve* pE);
int searchEleve (int pNum);
void lister();
